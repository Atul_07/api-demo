from flask import current_app as app
import os as __os

def get(key):
    return app.config.get(key)


def getall():
    return app.config


def get_env():
    __env_attrs = __os.environ
    env = __env_attrs.get('ENV')
    return env