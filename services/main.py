# import necessary libraries and linked module files.
from app import app

def function1():
	# Do something...
	app.logger.info("Inside Function 1 - GET")
	return {"msg" : "This is a response to GET request."}


def function2(content, session):
	# Do something using content...
	app.logger.info("Inside Function 2 - POST")
	if not "mobile" in content.keys() or content["mobile"] == {} or not content["mobile"]:
		content['mobile'] = None

	return {"name" : content["name"], "mobile" : content["mobile"], "msg" : "This is a response to POST request"}
