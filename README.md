# Flask- API demo

This flask API has two basic `GET` and `POST` requests, for demonstration purpose functions. Integration of DB is also possible in the same architecture. One can use any preferred python-SQL based libraries to create connection and communicate the data between program and DB. So, that has not been covered in this flow.

Note : I had tested and successfully run this demo API on **UBUNTU - 18.0** OS.


> This API will run on `localhost:6000` when run directly through **manage.py** file.
> If it is run using **supervisord**, then it will run on `IP_OF_server:5005` a/c to the currenct supervisor configurations.


## Pre-requisites:
- Anaconda with Python3.x installed
- Python 3.x - preferably, 3.6.9
- pip3 
- virtualenv (venv) - conda ENV

### 1. Part -1 : Setting the platform
- Create a virtual environment (outside the project repo) :
    ```
    conda create -n <YOUR_ENV> python=3.6 anaconda
    ```
- activate Virtual Environment :
    ```
    source activate <YOUR_ENV>
    [OR]
    source <PATH_TO_YOUR_ENV>/bin/activate
    ```
- Install necessary libraries : run this command at root directory of this repo.
    ```
    pip3 install -r requirements.txt
    ```
- Set ENV variable name in the system-variables :
    ```
    export ENV="local"
    ```
Note : *above command is subjective to OS*. Also, to avoid doing it multiple times. Add ENV var in the system variables file, manually. possible values of ENV - [local, base, qa, prod, etc] based on the chosen server.

- Change environment specific configurations for your project from the `config/` directory, in respective files. viz. `local.py` for **local** enviroment.
- To check the demo functioning : run this command at root directory of this repo
    ```
    gunicorn -w 3 --threads 20 -t 80 manage:app -b 0.0.0.0:5005 --log-level debug
    ```
- Hit the API with `GET` and `POST` requests. HURRAY ! You must receive some repsonse.



## Part - 2 : Setting up Supervisor on server/local -

- Install supervisor service.
    ```
    sudo apt install supervisor
    ```
- Run supervisor as a service.
    ```
    service supervisor start
    ```
- check supervisor service status.
    ```
    service supervisor status
    ```
- Create/Edit the supervisord-config file in supervisor_conf/ directory.
    * Change these fields a/c to the requirement -
        * program - change the name from demo to the 'program-name'
        * directory - absolute path of the program
        * environment - set adequate environemnt name
        * command - change number of threads, workers, etc, as per requirement.
        * user - replace with your user name
        * stdout_logfile / stderr_logfile - in case you want to change the log directory. ***Its recommended to keep the log_directory seperate, outside the project repo.***
- Give write permission to the project parent directory. - Use `chmod` and `chown`
- Similary give write permission to the logfile directory. - Use `chmod` and `chown`
- Go into the supervisor_conf directory. Run following commands.
    * ```supervisord``` - it will create the .sock file for this supervisord program.
    * give executable permission to supervisod.pid file, if already exists.
    ```chmod 764 supervisord.pid```
    * Run the supervisord config file.
    ```supervisord -c ./supervisord_demo.conf```
    * Open supervisor control panel.
    ```supervisorctl```
    * Boom ! You will see list of one or many flask programs running through supervisord. In our case, right now, program name would be "**demo**".
    * Access supervisor on web-browser (panel/ logs) via `localhost:9001` or `<IP_SERVER>:9001`.

Note : I am not sure right now, about how to enable username, password authentiction before entering into supervisor control panel on CMD. Please, read supervisord documentation for the reference.


## Expected response :

>> POST - 

```
{
    "mobile": "9876543210",
    "msg": "This is a response to POST request",
    "name": "mridal",
    "t_T_T_0": "8.234399999995645e-05 SECOND",
    "t_T_T_1": "8.797645568847656e-05 SECOND"
}
```

>> GET - 

```
{
    "msg": "This is a response to GET request.",
    "t_T_T_0": "0.0001063849999998201 SECOND",
    "t_T_T_1": "0.00011205673217773438 SECOND"
}
```

#### I hope it helps you run the program flawlessly, with minimum efforts.

### Thank You !


