from flask_script import Manager, Server
from flask_migrate import Migrate
from flask import request
import xlrd
import logging
from flask_cors import CORS
import pandas as pd

from app import app, db


CORS(app)
migrate = Migrate(app)
manager = Manager(app)
# when you run the program locally on port 6000
manager.add_command('runserver', Server(host='0.0.0.0', port=6000, threaded=True))


@app.before_request	
def log_before_request_info():
	if not request.headers.get('host') == "10.100.4.168:5000" and not request.headers.get('host') == "172.16.0.118:5000":
		app.logger.debug("****************************************************************************************************************************")
		app.logger.info("Headers: {}".format(request.headers))
		app.logger.info("Body: {}".format(request.get_data()))
		app.logger.info("Body: {}".format(request.data))
		app.logger.info("Body: {}".format(request.get_json()))
		app.logger.info("Body: {}".format(request.args))
		print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")

		print("****************************************************************************************************************************")
		print("Headers: {}".format(request.headers))
		print("Body: {}".format(request.get_data()))


@app.after_request
def log_after_request_info(response):
	if not request.headers.get('host') == "10.100.4.168:5000" and not request.headers.get('host') == "172.16.0.118:5000":
		# app.logger.info("response: {}".format(response.get_data()))
		app.logger.debug("****************************************************************************************************************************")
		# print("response: {}".format(response.get_data()))
		print("****************************************************************************************************************************")
	return response


	

if __name__ != '__main__':
	gunicorn_logger = logging.getLogger('gunicorn.error')
	app.logger.handlers = gunicorn_logger.handlers
	app.logger.setLevel(gunicorn_logger.level)

else:
	manager.run()
