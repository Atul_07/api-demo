import os as __os
from flask import Flask


app = Flask(__name__)
app.config.from_object('config.base')

# override by environment specific configuration files
__env_attrs = __os.environ
env = __env_attrs.get('ENV')

if not env:
    raise Exception("set {ENV} environment variable to start application")
elif env == "qa":
    import config.qa
    app.config.from_object(config.qa)
elif env == "prod":
    import config.prod
    app.config.from_object(config.prod)
elif env == "local":
    import config.local
    app.config.from_object(config.local)

# override by external environment configuration file
app.config.from_envvar('APP_SETTINGS', silent=True)

############ configure database if used...############
from connectors.mysql.mysql_connector import db
db.init_app(app)

############ register blueprints ############
from rest import api_controller

# add your rest API as a blueprint
app.register_blueprint(api_controller.api_blueprint)
