from app import app
from flask import Blueprint, jsonify, request
from services import main
import time
import gc

api_blueprint = Blueprint('api_controller', __name__)

## Initiate DB session if used.
from connectors.mysql.mysql_connector import db
session = db.session



@api_blueprint.route('/get_request', methods=['GET'])
def request1():
	starting_time_0_0_0_0_time = time.time()
	starting_time_0_0_0_0_process = time.process_time()

	app.logger.info("GET REQUEST INITIATED TIME = {}SECOND".format(time.time() - starting_time_0_0_0_0_time))

	output = main.function1()

	output["t_T_T_0"] = str(time.process_time() - starting_time_0_0_0_0_process) + " " + "SECOND"
	output["t_T_T_1"] = str(time.time() - starting_time_0_0_0_0_time) + " " + "SECOND"
	app.logger.info("T_T_T_0 API_TIME = {}SECOND".format(time.time() - starting_time_0_0_0_0_time))
	app.logger.info("T_T_T_0 API_PROCESS = {}SECOND".format(time.process_time() - starting_time_0_0_0_0_process))

	output = jsonify(output)

	gc.collect()

	app.logger.info("response: {}".format(output.get_data()))
	return output


@api_blueprint.route('/post_request', methods=['POST'])
def request2():
	starting_time_0_0_0_0_time = time.time()
	starting_time_0_0_0_0_process = time.process_time()

	content = request.get_json()
	app.logger.info("POST REQUEST INITIATED TIME  = {}SECOND".format(time.time() - starting_time_0_0_0_0_time))

	if not "name" in content.keys() or content["name"] == {} or not content["name"]:
		output = {"success": False, "msg": "Name not provided.",
				  "data": {"name": None, "mobile": None}}
	else:
		print("Start car based Credit Terms offer")
		output = main.function2(content, session)


	output["t_T_T_0"] = str(time.process_time() - starting_time_0_0_0_0_process) + " " + "SECOND"
	output["t_T_T_1"] = str(time.time() - starting_time_0_0_0_0_time) + " " + "SECOND"
	app.logger.info("T_T_T_0 API_TIME = {}SECOND".format(time.time() - starting_time_0_0_0_0_time))
	app.logger.info("T_T_T_0 API_PROCESS = {}SECOND".format(time.process_time() - starting_time_0_0_0_0_process))

	output = jsonify(output)

	gc.collect()

	app.logger.info("response: {}".format(output.get_data()))
	return output